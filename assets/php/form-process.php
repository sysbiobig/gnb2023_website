<?php

$errorMSG = "";

// NAME
if (empty($_POST["firstname"])) {
    $errorMSG = "First name is required ";
} else {
    $firstname = $_POST["firstname"];
}

if (empty($_POST["lastname"])) {
    $errorMSG = "Last name is required ";
} else {
    $lastname = $_POST["lastname"];
}

// EMAIL
if (empty($_POST["email"])) {
    $errorMSG .= "Email is required ";
} else {
    $email = $_POST["email"];
}

// Organization
$message = $_POST["message"];


// MSG SUBJECT
if (empty($_POST["organization"])) {
    $errorMSG .= "Organization is required ";
} else {
    $organization = $_POST["organization"];
}

$EmailTo = "franzpx125@gmail.com";
$Subject = "GNB2020 Registered speaker";

// prepare email body text
$Body = "";
$Body .= $email;
$Body .= " | ";
$Body .= $firstname;
$Body .= " | ";
$Body .= $lastname;
$Body .= " | ";
$Body .= $organization;
$Body .= " | ";
$Body .= $message;
$Body .= "\n";

// send email
//$success = mail($EmailTo, $Subject, $Body, "From: flaviano.londero@dia.units.it");

$success = file_put_contents("/var/www/gnb2020.units.it/LOGS/speakers.log", $Body, FILE_APPEND);


// redirect to success page
if ($success){
   echo "Thank you for registering to GNB2020.";
}else{
    if($errorMSG == ""){
        echo "Something went wrong :(";
    } else {
        echo $errorMSG;
    }
}

?>